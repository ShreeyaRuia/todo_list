const pending = document.querySelector('.Pending');
const completed = document.querySelector('.Completed');
let counter = 1;
let taskId = [];

function addTask(e) {
    e.preventDefault();
    const newtask = document.getElementById('addTask').value.trim();
    const newList = document.createElement('li');
    newList.id = "task" + counter;
    const doneBtn = document.createElement('button');
    const deleteBtn = document.createElement('button');
    const flagBtn = document.createElement('button');
    const textFeild = document.getElementById("addTask");

    doneBtn.innerHTML = '<i class="fa fa-check"></i>';
    deleteBtn.innerHTML = '<i class="fa fa-trash"></i>';
    flagBtn.innerHTML = '<i class="fa fa-flag"></i>';

    if (newtask.length) {
        textFeild.value = "";
        counter += 1
        textFeild.style = "default"
        textFeild.placeholder = "Add New Task";
        newList.textContent = newtask;
        pending.appendChild(newList);
        newList.appendChild(doneBtn);
        newList.appendChild(deleteBtn);
        newList.appendChild(flagBtn);
    } else {
        textFeild.placeholder = "Task can't be Empty! Add Something";
        textFeild.style.backgroundColor = "#f7022a"
        textFeild.style.color = "#fff"
    }

    flagBtn.addEventListener('click', function() {
        //console.log(taskId);
        const parent = this.parentNode;
        //console.log(parent);
        const index = taskId.indexOf(parent.id);
        //console.log(parent.id);
        if (index > -1) {
            //console.log("here");
            parent.style = "default"
            taskId.splice(index, 1);
        } else {
            taskId.push(parent.id);
            parent.style.backgroundColor = "#33A7FF"
        }
    });

    doneBtn.addEventListener('click', function() {
        const parent = this.parentNode;
        //parent.style.backgroundColor="#3CB371";
        parent.remove();
        completed.appendChild(parent);
        doneBtn.style.display = 'none';
        flagBtn.style.display = 'none';
    });

    deleteBtn.addEventListener('click', function() {
        const parent = this.parentNode;
        parent.remove();
    });
}



addBtn.addEventListener('click', addTask);
